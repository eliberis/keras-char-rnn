#!/usr/bin/env python
from nltk.corpus import brown
from nltk.corpus import stopwords

from keras.layers import *
from keras.models import Model, Sequential
from keras.callbacks import Callback
from keras.utils import to_categorical

import numpy as np


class DataLoader(object):
    EOW_TOKEN = '%'    
    NUM_LETTERS = 27
    
    @staticmethod
    def letter_to_idx(char):
        assert 'a' <= char <= 'z' or char == DataLoader.EOW_TOKEN
        return 0 if char == DataLoader.EOW_TOKEN else ord(char) - ord('a') + 1
    
    @staticmethod
    def idx_to_letter(idx):
        assert 0 <= idx <= DataLoader.NUM_LETTERS
        return DataLoader.EOW_TOKEN if idx == 0 else chr(idx - 1 + ord('a'))
    
    def __init__(self, words):
        self.words = self._preprocess_words(words)
        self.max_length = max(len(w) for w in self.words)
    
    def _preprocess_words(self, words):
        stops = set(stopwords.words('english'))
        return [w.lower() + DataLoader.EOW_TOKEN for w in words if w.isalpha() and w not in stops]

    def get(self):
        X = np.zeros((len(self.words), self.max_length, DataLoader.NUM_LETTERS))
        Y = np.zeros((len(self.words), self.max_length, DataLoader.NUM_LETTERS))
        for i, w in enumerate(self.words):
            chars = list(w)
            x = to_categorical([DataLoader.letter_to_idx(c) for c in chars[:-1]], 
                               num_classes=DataLoader.NUM_LETTERS)
            y = to_categorical([DataLoader.letter_to_idx(c) for c in chars[1:]], 
                               num_classes=DataLoader.NUM_LETTERS)
            X[i, :len(x)] = x
            Y[i, :len(y)] = y
        return X, Y

    
class Sampler(Callback):
    def __init__(self, temp=1.0):
        self.temp = temp
        
    def _sample_from_prob(self, p):
        p = np.asarray(p).astype('float64')
        p = np.log(p) / self.temp
        ex_p = np.exp(p)
        p = ex_p / np.sum(ex_p)
        p[p < 0] = 0
        p = p / np.sum(p)
        p = np.random.multinomial(1, p)
        return np.argmax(p)
    
    def _generate_a_word(self):
        word = [np.random.randint(1, DataLoader.NUM_LETTERS)]
        last_char = DataLoader.idx_to_letter(word[0])
        
        while last_char != DataLoader.EOW_TOKEN:
            inp = to_categorical(word, num_classes=DataLoader.NUM_LETTERS)
            ps = self.model.predict(np.expand_dims(inp, axis=0), verbose=0)[0, -1, :]
            idx = self._sample_from_prob(ps)
            word.append(idx)
            last_char = DataLoader.idx_to_letter(idx)
            
        return "".join([DataLoader.idx_to_letter(i) for i in word[:-1]])
            
    def on_epoch_end(self, epoch, logs={}):
        print("Words sampled at t =", self.temp, [self._generate_a_word() for _ in range(8)])

        
def get_model(num_classes):
    i = Input(shape=(None, num_classes))
    x = Masking()(i)
    x = LSTM(192, dropout=0.35, recurrent_dropout=0.1, return_sequences=True)(x)
    x = TimeDistributed(Dense(num_classes, activation='softmax'))(x)
    
    m = Model(inputs=i, outputs=x)
    m.compile(loss='categorical_crossentropy', optimizer='adam',
                  metrics=['accuracy'])
    return m


if __name__=="__main__":
    dl = DataLoader(brown.words(categories='editorial'))
    X, Y = dl.get()
    model = get_model(DataLoader.NUM_LETTERS)
    model.fit(X, Y, batch_size=64, epochs=50, validation_split=0.1, 
              callbacks=[Sampler(temp=0.1), Sampler(temp=0.5), Sampler(temp=1.0), Sampler(temp=2.0), Sampler(temp=3.0)])
    model.save_weights("sample.h5")
